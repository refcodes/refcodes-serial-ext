// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.ActionEvent;
import org.refcodes.serial.DecoratorSegment;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SerialSchema;
import org.refcodes.serial.TransmissionException;
import org.refcodes.serial.ext.observer.ObservableSectionDecorator.TransmissionObservable;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link ObservableSegment} class enriches a {@link Segment} decoratee with
 * observer functionality as of the {@link ObservableSegment} interface. In case
 * the decoratee was updated as of
 * {@link #receiveFrom(InputStream, OutputStream)} or
 * {@link #fromTransmission(Sequence)} or the like, a {@link TransmissionEvent}
 * is fired.
 *
 * @param <DECORATEE> The type of the observed decoratee in question.
 */
public class ObservableSegmentDecorator<DECORATEE extends Segment> implements ObservableSegment<DECORATEE>, DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TransmssionObservable _observable;
	private DECORATEE _decoratee;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using a default {@link ExecutorService} and dispatching
	 * events in sequentially (as of {@link ExecutionStrategy#SEQUENTIAL}).
	 *
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 */
	public ObservableSegmentDecorator( DECORATEE aDecoratee ) {
		this( aDecoratee, null, null );
	}

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using the provided {@link ExecutionStrategy} when
	 * dispatching the events.
	 * 
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 */
	public ObservableSegmentDecorator( DECORATEE aDecoratee, ExecutionStrategy aExecutionStrategy ) {
		_decoratee = aDecoratee;
		_observable = new TransmssionObservable( aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using the provided {@link ExecutorService} when dispatching
	 * events in parallel (as of {@link ExecutionStrategy#PARALLEL} and
	 * {@link ExecutionStrategy#JOIN}) and using the given
	 * {@link ExecutionStrategy} when dispatching the events.
	 * 
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutorService the executor service to use when dispatching
	 *        events.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 */
	public ObservableSegmentDecorator( DECORATEE aDecoratee, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		_decoratee = aDecoratee;
		_observable = new TransmssionObservable( aExecutorService, aExecutionStrategy );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final int theOffset = _decoratee.fromTransmission( aSequence, aOffset );
		try {
			_observable.fireEvent( new TransmissionEvent<>( _decoratee, this ) );
		}
		catch ( VetoException ignore ) { /* Cannot happen here */ }
		return theOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		_decoratee.receiveFrom( aInputStream, aReturnStream );
		try {
			_observable.fireEvent( new TransmissionEvent<>( _decoratee, this ) );
		}
		catch ( VetoException ignore ) { /* Cannot happen here */ }
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_decoratee.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( ObservableSegmentDecorator.class, "The observable payload segment decorator makes a payload segment observable.", _decoratee.toSchema() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( TransmissionObserver<DECORATEE> aObserver ) {
		return _observable.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( TransmissionObserver<DECORATEE> aObserver ) {
		return _observable.subscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( TransmissionObserver<DECORATEE> aObserver ) {
		return _observable.unsubscribeObserver( aObserver );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link TransmissionObservable} extends the {@link AbstractObservable}
	 * to make the using class {@link ActionEvent} aware.
	 */
	class TransmssionObservable extends AbstractObservable<TransmissionObserver<DECORATEE>, TransmissionEvent<DECORATEE>> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private ExecutionStrategy _executionStrategy;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new connection observable.
		 */
		public TransmssionObservable() {
			_executionStrategy = ExecutionStrategy.SEQUENTIAL;
		}

		/**
		 * Instantiates a new connection observable.
		 *
		 * @param aExecutionStrategy the execution strategy
		 */
		public TransmssionObservable( ExecutionStrategy aExecutionStrategy ) {
			this( null, aExecutionStrategy );
		}

		/**
		 * Instantiates a new connection observable.
		 *
		 * @param aExecutorService the executor service
		 * @param aExecutionStrategy the execution strategy
		 */
		public TransmssionObservable( ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
			super( aExecutorService );
			_executionStrategy = ( aExecutionStrategy != null ) ? aExecutionStrategy : ExecutionStrategy.SEQUENTIAL;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int size() {
			return super.size();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void clear() {
			super.clear();
		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Same as {@link #fireEvent(MetaDataEvent<?>, ExecutionStrategy)} with
		 * a predefined {@link ExecutionStrategy#JOIN}.
		 *
		 * @param aEvent the event
		 * 
		 * @return true, if successful
		 * 
		 * @throws VetoException the veto exception
		 * 
		 * @see #fireEvent(MetaDataEvent<?>, ExecutionStrategy)
		 */
		protected boolean fireEvent( TransmissionEvent<DECORATEE> aEvent ) throws VetoException {
			return super.fireEvent( aEvent, _executionStrategy );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected boolean fireEvent( TransmissionEvent<DECORATEE> aEvent, TransmissionObserver<DECORATEE> aObserver, ExecutionStrategy aExecutionStrategy ) throws Exception {
			aObserver.onEvent( aEvent );
			return true;
		}
	}
}
