// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import org.refcodes.mixin.PayloadAccessor;

/**
 * The {@link PayloadEvent} gets triggered by
 * {@link ObservablePayloadTransmission} instances such as the
 * {@link ObservablePayloadSegmentDecorator} or the
 * {@link ObservablePayloadSectionDecorator} upon a payload being received.
 *
 * @param <P> The type of the payload in question.
 */
public class PayloadEvent<P> extends SerialEvent<ObservablePayloadTransmission<P>> implements PayloadAccessor<P> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final P _payload;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an event with the given source.
	 *
	 * @param aPayload The payload to be carried by the event.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadEvent( P aPayload, ObservablePayloadTransmission<P> aSource ) {
		super( aSource );
		_payload = aPayload;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P getPayload() {
		return _payload;
	}
}
