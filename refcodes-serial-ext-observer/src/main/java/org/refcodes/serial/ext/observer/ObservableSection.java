// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import org.refcodes.observer.Observable;
import org.refcodes.serial.Section;

/**
 * The {@link ObservableSection} implements the {@link Observable} interface in
 * order to fire events upon a transmission being received (as of
 * {@link #receiveFrom(java.io.InputStream, int, java.io.OutputStream)} or
 * {@link #fromTransmission(org.refcodes.serial.Sequence, int)} or the like).
 * 
 * @param <T> The type of the transmission in question.
 */
public interface ObservableSection<T extends Section> extends Section, ObservableTransmission<T> {}
