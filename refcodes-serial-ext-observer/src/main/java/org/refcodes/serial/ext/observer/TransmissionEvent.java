// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import org.refcodes.serial.Transmission;
import org.refcodes.serial.TransmissionAccessor;

/**
 * The {@link TransmissionEvent} gets triggered by
 * {@link ObservableTransmission} instances such as the
 * {@link ObservableSegmentDecorator} or the {@link ObservableSectionDecorator}
 * upon a transmission being received.
 *
 * @param <T> The type of the transmission in question.
 */
public class TransmissionEvent<T extends Transmission> extends SerialEvent<ObservableTransmission<T>> implements TransmissionAccessor<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final T _trasmssion;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an event with the given source.
	 *
	 * @param aTransmssion The transmission to be carried by the event.
	 * @param aSource The source from which this event originated.
	 */
	public TransmissionEvent( T aTransmssion, ObservableTransmission<T> aSource ) {
		super( aSource );
		_trasmssion = aTransmssion;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getTransmission() {
		return _trasmssion;
	}
}
