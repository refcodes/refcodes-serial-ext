// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.serial.BreakerSectionDecorator;
import org.refcodes.serial.BreakerSegmentDecorator;
import org.refcodes.serial.PayloadSection;
import org.refcodes.serial.PayloadSegment;
import org.refcodes.serial.Section;
import org.refcodes.serial.Segment;
import org.refcodes.serial.SerialSugar;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the construction of observable various
 * {@link Segment} or {@link Section} type instances (and the like).
 */
public class ObservableSerialSugar extends SerialSugar {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new observable serial sugar.
	 */
	protected ObservableSerialSugar() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link ObservablePayloadSection} wrapping the given
	 * {@link PayloadSection} decoratee and using a default
	 * {@link ExecutorService} and dispatching events in sequentially (as of
	 * {@link ExecutionStrategy#SEQUENTIAL}).
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 *
	 * @return The accordingly created {@link BreakerSectionDecorator}.
	 */
	public static <T> ObservablePayloadSectionDecorator<T> observablePayloadSectionDecorator( PayloadSection<T> aDecoratee ) {
		return new ObservablePayloadSectionDecorator<>( aDecoratee );
	}

	/**
	 * Constructs a {@link ObservablePayloadSection} wrapping the given
	 * {@link PayloadSection} decoratee and using the provided
	 * {@link ExecutionStrategy} when dispatching the events.
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 *
	 * @return The accordingly created {@link BreakerSectionDecorator}.
	 */
	public static <T> ObservablePayloadSectionDecorator<T> observablePayloadSectionDecorator( PayloadSection<T> aDecoratee, ExecutionStrategy aExecutionStrategy ) {
		return new ObservablePayloadSectionDecorator<>( aDecoratee, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservablePayloadSection} wrapping the given
	 * {@link PayloadSection} decoratee and using the provided
	 * {@link ExecutorService} when dispatching events in parallel (as of
	 * {@link ExecutionStrategy#PARALLEL} and {@link ExecutionStrategy#JOIN})
	 * and using the given {@link ExecutionStrategy} when dispatching the
	 * events.
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutorService the executor service to use when dispatching
	 *        events.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 *
	 * @return The accordingly created {@link BreakerSectionDecorator}.
	 */
	public static <T> ObservablePayloadSectionDecorator<T> observablePayloadSectionDecorator( PayloadSection<T> aDecoratee, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		return new ObservablePayloadSectionDecorator<>( aDecoratee, aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservablePayloadSegment} wrapping the given
	 * {@link PayloadSegment} decoratee and using a default
	 * {@link ExecutorService} and dispatching events in sequentially (as of
	 * {@link ExecutionStrategy#SEQUENTIAL}).
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 *
	 * @return The accordingly created {@link BreakerSegmentDecorator}.
	 */
	public static <T> ObservablePayloadSegmentDecorator<T> observablePayloadSegmentDecorator( PayloadSegment<T> aDecoratee ) {
		return new ObservablePayloadSegmentDecorator<>( aDecoratee );
	}

	/**
	 * Constructs a {@link ObservablePayloadSegment} wrapping the given
	 * {@link PayloadSegment} decoratee and using the provided
	 * {@link ExecutionStrategy} when dispatching the events.
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 *
	 * @return The accordingly created {@link BreakerSegmentDecorator}.
	 */
	public static <T> ObservablePayloadSegmentDecorator<T> observablePayloadSegmentDecorator( PayloadSegment<T> aDecoratee, ExecutionStrategy aExecutionStrategy ) {
		return new ObservablePayloadSegmentDecorator<>( aDecoratee, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservablePayloadSegment} wrapping the given
	 * {@link PayloadSegment} decoratee and using the provided
	 * {@link ExecutorService} when dispatching events in parallel (as of
	 * {@link ExecutionStrategy#PARALLEL} and {@link ExecutionStrategy#JOIN})
	 * and using the given {@link ExecutionStrategy} when dispatching the
	 * events.
	 *
	 * @param <T> The type of the payload.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutorService the executor service to use when dispatching
	 *        events.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 *
	 * @return The accordingly created {@link BreakerSegmentDecorator}.
	 */
	public static <T> ObservablePayloadSegmentDecorator<T> observablePayloadSegmentDecorator( PayloadSegment<T> aDecoratee, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		return new ObservablePayloadSegmentDecorator<>( aDecoratee, aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableSection} wrapping the given {@link Section}
	 * decoratee and using a default {@link ExecutorService} and dispatching
	 * events in sequentially (as of {@link ExecutionStrategy#SEQUENTIAL}).
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ObservableSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ObservableSectionDecorator<DECORATEE> observableSection( DECORATEE aDecoratee ) {
		return new ObservableSectionDecorator<>( aDecoratee );
	}

	/**
	 * Constructs a {@link ObservableSection} wrapping the given {@link Section}
	 * decoratee and using the provided {@link ExecutionStrategy} when
	 * dispatching the events.
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 * 
	 * @return The accordingly created {@link ObservableSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ObservableSectionDecorator<DECORATEE> observableSection( DECORATEE aDecoratee, ExecutionStrategy aExecutionStrategy ) {
		return new ObservableSectionDecorator<>( aDecoratee, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableSection} wrapping the given {@link Section}
	 * decoratee and using the provided {@link ExecutorService} when dispatching
	 * events in parallel (as of {@link ExecutionStrategy#PARALLEL} and
	 * {@link ExecutionStrategy#JOIN}) and using the given
	 * {@link ExecutionStrategy} when dispatching the events.
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutorService the executor service to use when dispatching
	 *        events.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 * 
	 * @return The accordingly created {@link ObservableSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ObservableSectionDecorator<DECORATEE> observableSection( DECORATEE aDecoratee, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		return new ObservableSectionDecorator<>( aDecoratee, aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using a default {@link ExecutorService} and dispatching
	 * events in sequentially (as of {@link ExecutionStrategy#SEQUENTIAL}).
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ObservableSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ObservableSegmentDecorator<DECORATEE> observableSegment( DECORATEE aDecoratee ) {
		return new ObservableSegmentDecorator<>( aDecoratee );
	}

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using the provided {@link ExecutionStrategy} when
	 * dispatching the events.
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 * 
	 * @return The accordingly created {@link ObservableSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ObservableSegmentDecorator<DECORATEE> observableSegment( DECORATEE aDecoratee, ExecutionStrategy aExecutionStrategy ) {
		return new ObservableSegmentDecorator<>( aDecoratee, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableSegment} wrapping the given {@link Segment}
	 * decoratee and using the provided {@link ExecutorService} when dispatching
	 * events in parallel (as of {@link ExecutionStrategy#PARALLEL} and
	 * {@link ExecutionStrategy#JOIN}) and using the given
	 * {@link ExecutionStrategy} when dispatching the events.
	 *
	 * @param <DECORATEE> The type of the observed decoratee in question.
	 * @param aDecoratee The decoratee to be wrapped by the observable
	 *        decorator.
	 * @param aExecutorService the executor service to use when dispatching
	 *        events.
	 * @param aExecutionStrategy the execution strategy to use when dispatching
	 *        events.
	 * 
	 * @return The accordingly created {@link ObservableSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ObservableSegmentDecorator<DECORATEE> observableSegment( DECORATEE aDecoratee, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		return new ObservableSegmentDecorator<>( aDecoratee, aExecutorService, aExecutionStrategy );
	}
}
