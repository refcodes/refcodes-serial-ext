module org.refcodes.serial.ext.observer {
	requires transitive org.refcodes.serial;
	requires transitive org.refcodes.observer;

	exports org.refcodes.serial.ext.observer;
}
