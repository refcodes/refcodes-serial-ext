package org.refcodes.serial.ext.observer;

import java.util.Arrays;

import org.refcodes.runtime.Execution;

public class TestFixures {

	// /////////////////////////////////////////////////////////////////////////
	// FIXURES:
	// /////////////////////////////////////////////////////////////////////////

	public static Values createValues() {
		final Values theValues = new Values();
		theValues._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theValues._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theValues._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		return theValues;
	}

	public static Wrapper createWrapper() {
		final Wrapper theWrapper = new Wrapper();
		theWrapper._booleans = new Boolean[] { true, false, true, false, true, false, true, false, true, false };
		theWrapper._bytes = new Byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theWrapper._chars = new Character[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theWrapper._shorts = new Short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theWrapper._ints = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theWrapper._longs = new Long[] { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 0L };
		theWrapper._floats = new Float[] { 1F, 2F, 3F, 4F, 5F, 6F, 7F, 8F, 9F, 0F };
		theWrapper._doubles = new Double[] { 1D, 2D, 3D, 4D, 5D, 6D, 7D, 8D, 9D, 0D };
		theWrapper._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		if ( Execution.isUnderTest() ) {
			System.out.println( theWrapper );
		}
		return theWrapper;
	}

	public static Sensors createSensors() {
		// @formatter:off
		final Sensors theSensors = new Sensors(
			new Sensor( "TEMPX87", 17346 ),
			new Sensor( "TEMPX03", 14346 ),
			new Sensor( "TEMPY11", 9342 ),
			new Sensor( "AUXLC33", 63483 )
		);		// @formatter:on
		return theSensors;
	}

	public static SensorValues createSensorValues() {
		final SensorValues theSensorValues = new SensorValues();
		theSensorValues._valuesA._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theSensorValues._valuesA._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theSensorValues._valuesA._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesA._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		theSensorValues._valuesB._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theSensorValues._valuesB._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesB._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theSensorValues._valuesB._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesB._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesB._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesB._floats = new float[] { 1.1F, 2.2F, 3.3F, 4.4F, 5.5F, 6.6F, 7.7F, 8.8F, 9.9F, 0.0F };
		theSensorValues._valuesB._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesB._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		theSensorValues._valuesC._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theSensorValues._valuesC._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
		theSensorValues._valuesC._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesC._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		theSensorValues._valuesD._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theSensorValues._valuesD._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theSensorValues._valuesD._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theSensorValues._valuesD._strings = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
		return theSensorValues;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class SensorValues {

		public Values _valuesA = new Values();
		public Values _valuesB = new Values();
		public Values _valuesC = new Values();
		public Values _valuesD = new Values();

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "SensorValues [valuesA=" + _valuesA + ", valuesB=" + _valuesB + ", valuesC=" + _valuesC + ", valuesD=" + _valuesD + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( _valuesA == null ) ? 0 : _valuesA.hashCode() );
			result = prime * result + ( ( _valuesB == null ) ? 0 : _valuesB.hashCode() );
			result = prime * result + ( ( _valuesC == null ) ? 0 : _valuesC.hashCode() );
			result = prime * result + ( ( _valuesD == null ) ? 0 : _valuesD.hashCode() );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final SensorValues other = (SensorValues) obj;
			if ( _valuesA == null ) {
				if ( other._valuesA != null ) {
					return false;
				}
			}
			else if ( !_valuesA.equals( other._valuesA ) ) {
				return false;
			}
			if ( _valuesB == null ) {
				if ( other._valuesB != null ) {
					return false;
				}
			}
			else if ( !_valuesB.equals( other._valuesB ) ) {
				return false;
			}
			if ( _valuesC == null ) {
				if ( other._valuesC != null ) {
					return false;
				}
			}
			else if ( !_valuesC.equals( other._valuesC ) ) {
				return false;
			}
			if ( _valuesD == null ) {
				if ( other._valuesD != null ) {
					return false;
				}
			}
			else if ( !_valuesD.equals( other._valuesD ) ) {
				return false;
			}
			return true;
		}
	}

	public static class Values {

		public boolean[] _booleans;
		public byte[] _bytes;
		public char[] _chars;
		public short[] _shorts;
		public int[] _ints;
		public long[] _longs;
		public float[] _floats;
		public double[] _doubles;
		public String[] _strings;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Values [booleans=" + Arrays.toString( _booleans ) + ", bytes=" + Arrays.toString( _bytes ) + ", chars=" + Arrays.toString( _chars ) + ", short=" + Arrays.toString( _shorts ) + ", ints=" + Arrays.toString( _ints ) + ", longs=" + Arrays.toString( _longs ) + ", floats=" + Arrays.toString( _floats ) + ", doubles=" + Arrays.toString( _doubles ) + ", strings=" + Arrays.toString( _strings ) + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( _booleans );
			result = prime * result + Arrays.hashCode( _bytes );
			result = prime * result + Arrays.hashCode( _chars );
			result = prime * result + Arrays.hashCode( _doubles );
			result = prime * result + Arrays.hashCode( _ints );
			result = prime * result + Arrays.hashCode( _longs );
			result = prime * result + Arrays.hashCode( _shorts );
			result = prime * result + Arrays.hashCode( _strings );
			result = prime * result + Arrays.hashCode( _floats );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Values other = (Values) obj;
			if ( !Arrays.equals( _booleans, other._booleans ) ) {
				return false;
			}
			if ( !Arrays.equals( _bytes, other._bytes ) ) {
				return false;
			}
			if ( !Arrays.equals( _chars, other._chars ) ) {
				return false;
			}
			if ( !Arrays.equals( _doubles, other._doubles ) ) {
				return false;
			}
			if ( !Arrays.equals( _ints, other._ints ) ) {
				return false;
			}
			if ( !Arrays.equals( _longs, other._longs ) ) {
				return false;
			}
			if ( !Arrays.equals( _shorts, other._shorts ) ) {
				return false;
			}
			if ( !Arrays.equals( _strings, other._strings ) ) {
				return false;
			}
			if ( !Arrays.equals( _floats, other._floats ) ) {
				return false;
			}
			return true;
		}
	}

	public static class Wrapper {

		public Boolean[] _booleans;
		public Byte[] _bytes;
		public Character[] _chars;
		public Short[] _shorts;
		public Integer[] _ints;
		public Long[] _longs;
		public Float[] _floats;
		public Double[] _doubles;
		public String[] _strings;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Wrapper [booleans=" + Arrays.toString( _booleans ) + ", bytes=" + Arrays.toString( _bytes ) + ", chars=" + Arrays.toString( _chars ) + ", short=" + Arrays.toString( _shorts ) + ", ints=" + Arrays.toString( _ints ) + ", longs=" + Arrays.toString( _longs ) + ", floats=" + Arrays.toString( _floats ) + ", doubles=" + Arrays.toString( _doubles ) + ", strings=" + Arrays.toString( _strings ) + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( _booleans );
			result = prime * result + Arrays.hashCode( _bytes );
			result = prime * result + Arrays.hashCode( _chars );
			result = prime * result + Arrays.hashCode( _doubles );
			result = prime * result + Arrays.hashCode( _floats );
			result = prime * result + Arrays.hashCode( _ints );
			result = prime * result + Arrays.hashCode( _longs );
			result = prime * result + Arrays.hashCode( _shorts );
			result = prime * result + Arrays.hashCode( _strings );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Wrapper other = (Wrapper) obj;
			if ( !Arrays.equals( _booleans, other._booleans ) ) {
				return false;
			}
			if ( !Arrays.equals( _bytes, other._bytes ) ) {
				return false;
			}
			if ( !Arrays.equals( _chars, other._chars ) ) {
				return false;
			}
			if ( !Arrays.equals( _doubles, other._doubles ) ) {
				return false;
			}
			if ( !Arrays.equals( _floats, other._floats ) ) {
				return false;
			}
			if ( !Arrays.equals( _ints, other._ints ) ) {
				return false;
			}
			if ( !Arrays.equals( _longs, other._longs ) ) {
				return false;
			}
			if ( !Arrays.equals( _shorts, other._shorts ) ) {
				return false;
			}
			if ( !Arrays.equals( _strings, other._strings ) ) {
				return false;
			}
			return true;
		}
	}

	public static class SensorArray {
		public Sensor[] sensors;

		/**
		 * Instantiates a new sensor array.
		 */
		public SensorArray() {}

		/**
		 * Instantiates a new sensor array.
		 *
		 * @param sensors the sensors
		 */
		public SensorArray( Sensor[] sensors ) {
			this.sensors = sensors;
		}

		/**
		 * Gets the sensors.
		 *
		 * @return the sensors
		 */
		public Sensor[] getSensors() {
			return sensors;
		}

		/**
		 * Sets the sensors.
		 *
		 * @param sensors the new sensors
		 */
		public void setSensors( Sensor[] sensors ) {
			this.sensors = sensors;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( sensors );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final SensorArray other = (SensorArray) obj;
			if ( !Arrays.equals( sensors, other.sensors ) ) {
				return false;
			}
			return true;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "SensorArray [sensors=" + Arrays.toString( sensors ) + "]";
		}
	}

	public static class Sensor {

		public int _value;
		public String _name;

		/**
		 * Instantiates a new sensor.
		 */
		public Sensor() {}

		/**
		 * Instantiates a new sensor.
		 *
		 * @param aName the name
		 * @param aValue the value
		 */
		public Sensor( String aName, int aValue ) {
			_name = aName;
			_value = aValue;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return _name;
		}

		/**
		 * Gets the payload.
		 *
		 * @return the payload
		 */
		public int getPayload() {
			return _value;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Sensor [value=" + _value + ", name=" + _name + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( _name == null ) ? 0 : _name.hashCode() );
			result = prime * result + _value;
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Sensor other = (Sensor) obj;
			if ( _name == null ) {
				if ( other._name != null ) {
					return false;
				}
			}
			else if ( !_name.equals( other._name ) ) {
				return false;
			}
			return _value != other._value ? false : true;
		}
	}

	public static class Sensors {

		public Sensor _sensorA;
		public Sensor _sensorB;
		public Sensor _sensorC;
		public Sensor _sensorD;

		/**
		 * Instantiates a new sensors.
		 */
		public Sensors() {}

		/**
		 * Instantiates a new sensors.
		 *
		 * @param aSensorA the sensor A
		 * @param aSensorB the sensor B
		 * @param aSensorC the sensor C
		 * @param aSensorD the sensor D
		 */
		public Sensors( Sensor aSensorA, Sensor aSensorB, Sensor aSensorC, Sensor aSensorD ) {
			_sensorA = aSensorA;
			_sensorB = aSensorB;
			_sensorC = aSensorC;
			_sensorD = aSensorD;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "Sensors [sensorA=" + _sensorA + ", sensorB=" + _sensorB + ", sensorC=" + _sensorC + ", sensorD=" + _sensorD + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( _sensorA == null ) ? 0 : _sensorA.hashCode() );
			result = prime * result + ( ( _sensorB == null ) ? 0 : _sensorB.hashCode() );
			result = prime * result + ( ( _sensorC == null ) ? 0 : _sensorC.hashCode() );
			result = prime * result + ( ( _sensorD == null ) ? 0 : _sensorD.hashCode() );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Sensors other = (Sensors) obj;
			if ( _sensorA == null ) {
				if ( other._sensorA != null ) {
					return false;
				}
			}
			else if ( !_sensorA.equals( other._sensorA ) ) {
				return false;
			}
			if ( _sensorB == null ) {
				if ( other._sensorB != null ) {
					return false;
				}
			}
			else if ( !_sensorB.equals( other._sensorB ) ) {
				return false;
			}
			if ( _sensorC == null ) {
				if ( other._sensorC != null ) {
					return false;
				}
			}
			else if ( !_sensorC.equals( other._sensorC ) ) {
				return false;
			}
			if ( _sensorD == null ) {
				if ( other._sensorD != null ) {
					return false;
				}
			}
			else if ( !_sensorD.equals( other._sensorD ) ) {
				return false;
			}
			return true;
		}
	}

	protected static WeatherData createWeatherData() {
		final WeatherData theSensors = new WeatherData( new Sensor( "WIND01", 17346 ), new Sensor( "TEMP01", 14346 ), new Sensor( "HUM01", 9342 ), new Sensor( "WIND02", 978 ), new Sensor( "TEMP02", -878 ), new Sensor( "HUM02", -1 ) );
		return theSensors;
	}

	public static class WeatherData {

		private Sensor[] _sensors;

		/**
		 * Instantiates a new weather data.
		 */
		public WeatherData() {}

		/**
		 * Instantiates a new weather data.
		 *
		 * @param sensors the sensors
		 */
		public WeatherData( Sensor... sensors ) {
			_sensors = sensors;
		}

		/**
		 * Gets the sensors.
		 *
		 * @return the sensors
		 */
		public Sensor[] getSensors() {
			return _sensors;
		}

		/**
		 * Sets the sensors.
		 *
		 * @param sensors the new sensors
		 */
		public void setSensors( Sensor[] sensors ) {
			_sensors = sensors;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( _sensors );
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final WeatherData other = (WeatherData) obj;
			if ( !Arrays.equals( _sensors, other._sensors ) ) {
				return false;
			}
			return true;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return "WeatherData [sensors=" + Arrays.toString( _sensors ) + "]";
		}
	}
}