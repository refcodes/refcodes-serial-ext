// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.observer;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;
import static org.refcodes.serial.ext.observer.ObservableSerialSugar.*;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.ComplexTypeSegment;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortTestBench;
import org.refcodes.serial.Segment;
import org.refcodes.serial.ext.observer.TestFixures.Sensor;
import org.refcodes.serial.ext.observer.TestFixures.WeatherData;

public class ObservablePayloadTest extends AbstractPortTest implements PayloadObserver<WeatherData> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 3;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _hasEvent1; // Avoid thread race condition in case...  
	private boolean _hasEvent2; // ...tests are executed in parallel.

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testObservablePayloadSection() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final ObservablePayloadSection<String[]> theReceiverPayloadSec;
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = allocSegment( stringArraySection( thePayload ) );
				final Segment theReceiverSeg = allocSegment( theReceiverPayloadSec = observablePayloadSectionDecorator( stringArraySection(), ExecutionStrategy.PARALLEL ) );
				theReceiverPayloadSec.subscribeObserver( this::onPayload );
				for ( int i = 0; i < LOOPS; i++ ) {
					_hasEvent1 = false;
					theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					//	RetryTimeout theTimeout = new RetryTimeout( 1000, 8 );
					//	while ( !_hasEvent1 && theTimeout.nextRetry() ) {}
					synchronized ( this ) {
						wait();
					}
					if ( !_hasEvent1 ) {
						fail( "Expecting an event to be received!" );
					}
					final String[] theReceiverData = theReceiverPayloadSec.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( Arrays.toString( theReceiverData ) );
					}
					assertArrayEquals( thePayload, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testObservablePayloadSegment() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverComplexSegment;
				final ComplexTypeSegment<WeatherData> theSenderSeg = complexTypeSegment( theSenderData );
				final ObservablePayloadSegment<WeatherData> theReceiverSeg = observablePayloadSegmentDecorator( theReceiverComplexSegment = complexTypeSegment( WeatherData.class ), ExecutionStrategy.PARALLEL );
				theReceiverSeg.subscribeObserver( this );
				for ( int i = 0; i < LOOPS; i++ ) {
					_hasEvent2 = false;
					theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					//	RetryTimeout theTimeout = new RetryTimeout( 1000, 8 );
					//	while ( !_hasEvent2 && theTimeout.nextRetry() ) {}
					synchronized ( this ) {
						wait();
					}
					if ( !_hasEvent2 ) {
						fail( "Expecting an event to be received!" );
					}
					final WeatherData theReceiverData = theReceiverComplexSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	public void onPayload( PayloadEvent<String[]> aEvent ) {
		_hasEvent1 = true;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "> Received event: " + aEvent );
		}
		synchronized ( this ) {
			notifyAll();
		}
	}

	@Override
	public void onEvent( PayloadEvent<WeatherData> aEvent ) {
		_hasEvent2 = true;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "> Received event: " + aEvent );
		}
		synchronized ( this ) {
			notifyAll();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected static WeatherData createWeatherData() {
		final WeatherData theSensors = new WeatherData( new Sensor( "WIND01", 17346 ), new Sensor( "TEMP01", 14346 ), new Sensor( "HUM01", 9342 ), new Sensor( "WIND02", 978 ), new Sensor( "TEMP02", -878 ), new Sensor( "HUM02", -1 ) );
		return theSensors;
	}
}
