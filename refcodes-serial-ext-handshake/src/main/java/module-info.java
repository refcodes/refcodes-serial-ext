module org.refcodes.serial.ext.handshake {
	requires transitive org.refcodes.serial;
	requires java.logging;

	exports org.refcodes.serial.ext.handshake;
}
