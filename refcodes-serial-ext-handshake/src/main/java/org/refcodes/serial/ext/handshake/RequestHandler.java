// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

import org.refcodes.serial.Segment;

/**
 * A {@link RequestHandler} provides means to construct a response
 * {@link Segment} in response to an incoming request {@link Segment}.
 * 
 * @param <REQUEST> The {@link Segment} type to be handled.
 *
 */
public interface RequestHandler<REQUEST extends Segment> {

	/**
	 * The {@link #onRequest(Segment)} method processes a request and produces a
	 * response.
	 * 
	 * @param aRequest The according request {@link Segment} for which to
	 *        produce a response.
	 * 
	 * @return The produces response {@link Segment}.
	 */
	Segment onRequest( REQUEST aRequest );

}
