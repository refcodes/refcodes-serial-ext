// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.data.AckTimeout;
import org.refcodes.data.IoRetryCount;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;
import org.refcodes.serial.MagicBytes;
import org.refcodes.serial.SegmentPackager;
import org.refcodes.serial.TransmissionMetrics;

/**
 * The {@link HandshakeTransmissionMetrics} extend the
 * {@link TransmissionMetrics} with handshake specific metrics.
 */
public class HandshakeTransmissionMetrics extends TransmissionMetrics implements ReplyRetryNumberAccessor, ReplyTimeoutMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final byte[] DEFAULT_ACKNOWLEDGEABLE_TRANSMISSION_MAGIC_BYTES = MagicBytes.ACKNOWLEDGEABLE_TRANSMISSION.getMagicBytes();
	private static final byte[] DEFAULT_ACKNOWLEDGEABLE_RESPONSE_MAGIC_BYTES = MagicBytes.ACKNOWLEDGEABLE_RESPONSE.getMagicBytes();
	private static final byte[] DEFAULT_ACKNOWLEDGEABLE_REQUEST_MAGIC_BYTES = MagicBytes.ACKNOWLEDGEABLE_REQUEST.getMagicBytes();
	private static final byte[] DEFAULT_TRANSMISSION_MAGIC_BYTES = MagicBytes.TRANSMISSION.getMagicBytes();
	private static final byte[] DEFAULT_RESPONSE_MAGIC_BYTES = MagicBytes.RESPONSE.getMagicBytes();
	private static final byte[] DEFAULT_RESPONSE_ACKNOWLEDGE_MAGIC_BYTES = MagicBytes.RESPONSE_ACKNOWLEDGE.getMagicBytes();
	private static final byte[] DEFAULT_REQUEST_MAGIC_BYTES = MagicBytes.REQUEST.getMagicBytes();
	private static final byte[] DEFAULT_TRANSMISSION_DISMISSED_MAGIC_BYTES = MagicBytes.TRANSMISSION_DISMISSED.getMagicBytes();
	private static final byte[] DEFAULT_REQUEST_DISMISSED_MAGIC_BYTES = MagicBytes.REQUEST_DISMISSED.getMagicBytes();

	public static final int DEFAULT_REPLY_RETRY_NUMBER = IoRetryCount.NORM.getValue();
	public static final long DEFAULT_REPLY_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private byte[] _acknowledgeableTransmissionMagicBytes = DEFAULT_ACKNOWLEDGEABLE_TRANSMISSION_MAGIC_BYTES;
	private byte[] _acknowledgeableResponseMagicBytes = DEFAULT_ACKNOWLEDGEABLE_RESPONSE_MAGIC_BYTES;
	private byte[] _acknowledgeableRequestMagicBytes = DEFAULT_ACKNOWLEDGEABLE_REQUEST_MAGIC_BYTES;
	private byte[] _transmissionMagicBytes = DEFAULT_TRANSMISSION_MAGIC_BYTES;
	private byte[] _responseMagicBytes = DEFAULT_RESPONSE_MAGIC_BYTES;
	private byte[] _responseAcknowledgeMagicBytes = DEFAULT_RESPONSE_ACKNOWLEDGE_MAGIC_BYTES;
	private byte[] _requestMagicBytes = DEFAULT_REQUEST_MAGIC_BYTES;
	private byte[] _transmissionDismissedMagicBytes = DEFAULT_TRANSMISSION_DISMISSED_MAGIC_BYTES;
	private byte[] _requestDismissedMagicBytes = DEFAULT_REQUEST_DISMISSED_MAGIC_BYTES;

	protected int _replyRetryNumber = DEFAULT_REPLY_RETRY_NUMBER;
	protected long _replyTimeoutInMs = DEFAULT_REPLY_TIMEOUT_IN_MS;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new handshake transmission metrics.
	 *
	 * @param aBuilder the a builder
	 */
	protected HandshakeTransmissionMetrics( Builder aBuilder ) {
		super( aBuilder );
		_acknowledgeableRequestMagicBytes = aBuilder.acknowledgeableRequestMagicBytes;
		_acknowledgeableResponseMagicBytes = aBuilder.acknowledgeableResponseMagicBytes;
		_acknowledgeableTransmissionMagicBytes = aBuilder.acknowledgeableTransmissionMagicBytes;
		_requestMagicBytes = aBuilder.requestMagicBytes;
		_responseMagicBytes = aBuilder.responseMagicBytes;
		_responseAcknowledgeMagicBytes = aBuilder.responseAcknowledgeMagicBytes;
		_transmissionMagicBytes = aBuilder.transmissionMagicBytes;
		_requestDismissedMagicBytes = aBuilder.requestDismissedMagicBytes;
		_transmissionDismissedMagicBytes = aBuilder.transmissionDismissedMagicBytes;
		_replyRetryNumber = aBuilder.replyRetryNumber;
		_replyTimeoutInMs = aBuilder.replyTimeoutInMs;
		validateMagicBytes();
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates an instance of the {@link HandshakeTransmissionMetrics} with
	 * default values being applied as defined in the
	 * {@link HandshakeTransmissionMetrics} type.
	 */
	public HandshakeTransmissionMetrics() {
		validateMagicBytes();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getReplyRetryNumber() {
		return _replyRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReplyTimeoutMillis() {
		return _replyTimeoutInMs;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getAcknowledgeableTransmissionMagicBytes() {
		return _acknowledgeableTransmissionMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getAcknowledgeableResponseMagicBytes() {
		return _acknowledgeableResponseMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getAcknowledgeableRequestMagicBytes() {
		return _acknowledgeableRequestMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getTransmissionMagicBytes() {
		return _transmissionMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getResponseMagicBytes() {
		return _responseMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getResponseAcknowledgeMagicBytes() {
		return _responseAcknowledgeMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getRequestMagicBytes() {
		return _requestMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getTransmissionDismissedMagicBytes() {
		return _transmissionDismissedMagicBytes;
	}

	/**
	 * Retrieves the according magic bytes.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getRequestDismissedMagicBytes() {
		return _requestDismissedMagicBytes;
	}

	/**
	 * 
	 * Determines the {@link AcknowledgeType} from the provided magic bytes.
	 * 
	 * @param aMagicBytes The magic bytes for which to get the
	 *        {@link AcknowledgeType}.
	 * 
	 * @return The according {@link AcknowledgeType}.
	 * 
	 * @throws IllegalArgumentException thrown in case the magic bytes did not
	 *         match any {@link AcknowledgeType}.
	 */
	public AcknowledgeType toAcknowledgeType( byte[] aMagicBytes ) {
		if ( Arrays.equals( getAcknowledgeMagicBytes(), aMagicBytes ) ) {
			return AcknowledgeType.ACKNOWLEDGE;
		}
		if ( Arrays.equals( getResponseAcknowledgeMagicBytes(), aMagicBytes ) ) {
			return AcknowledgeType.RESPONSE;
		}
		if ( Arrays.equals( getTransmissionDismissedMagicBytes(), aMagicBytes ) ) {
			return AcknowledgeType.TRANSMISSION_DISMISSED;
		}
		if ( Arrays.equals( getRequestDismissedMagicBytes(), aMagicBytes ) ) {
			return AcknowledgeType.REQUEST_DISMISSED;
		}
		if ( Arrays.equals( getPongMagicBytes(), aMagicBytes ) ) {
			return AcknowledgeType.PONG;
		}
		throw new IllegalArgumentException( "The provided magic bytes <" + aMagicBytes.length != null ? Arrays.toString( aMagicBytes ) : null + "> did not match any <" + AcknowledgeType.class.getSimpleName() + ">!" );
	}

	/**
	 * Determines the magic bytes for the given {@link AcknowledgeType}.
	 * 
	 * @param aAcknowledgeType The {@link AcknowledgeType} for which to retrieve
	 *        the magic bytes.
	 * 
	 * @return The according magic bytes or null in case of the
	 *         {@link AcknowledgeType} being null.
	 */
	public byte[] toMagicBytes( AcknowledgeType aAcknowledgeType ) {
		switch ( aAcknowledgeType ) {
		case ACKNOWLEDGE -> {
			return getAcknowledgeMagicBytes();
		}
		case REQUEST_DISMISSED -> {
			return getRequestDismissedMagicBytes();
		}
		case RESPONSE -> {
			return getResponseAcknowledgeMagicBytes();
		}
		case TRANSMISSION_DISMISSED -> {
			return getTransmissionDismissedMagicBytes();
		}
		case PONG -> {
			return getPongMagicBytes();
		}
		}
		return null;
	}

	/**
	 * 
	 * Determines the {@link TransmissionType} from the provided magic bytes.
	 * 
	 * @param aMagicBytes The magic bytes for which to get the
	 *        {@link TransmissionType}.
	 * 
	 * @return The according {@link TransmissionType}.
	 * 
	 * @throws IllegalArgumentException thrown in case the magic bytes did not
	 *         match any {@link TransmissionType}.
	 */
	public TransmissionType toTransmissionType( byte[] aMagicBytes ) {
		if ( Arrays.equals( getAcknowledgeableRequestMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.ACKNOWLEDGEABLE_REQUEST;
		}
		if ( Arrays.equals( getAcknowledgeableResponseMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.ACKNOWLEDGEABLE_RESPONSE;
		}
		if ( Arrays.equals( getAcknowledgeableTransmissionMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.ACKNOWLEDGEABLE_TRANSMISSION;
		}
		if ( Arrays.equals( getRequestMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.REQUEST;
		}
		if ( Arrays.equals( getResponseMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.RESPONSE;
		}
		if ( Arrays.equals( getTransmissionMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.TRANSMISSION;
		}
		if ( Arrays.equals( getPingMagicBytes(), aMagicBytes ) ) {
			return TransmissionType.PING;
		}
		throw new IllegalArgumentException( "The provided magic bytes <" + aMagicBytes.length != null ? Arrays.toString( aMagicBytes ) : null + "> did not match any <" + TransmissionType.class.getSimpleName() + ">!" );
	}

	/**
	 * Determines the magic bytes for the given {@link TransmissionType}.
	 * 
	 * @param aTransmissionType The {@link TransmissionType} for which to
	 *        retrieve the magic bytes.
	 * 
	 * @return The according magic bytes or null in case of the
	 *         {@link TransmissionType} being null.
	 */
	public byte[] toMagicBytes( TransmissionType aTransmissionType ) {
		switch ( aTransmissionType ) {
		case ACKNOWLEDGEABLE_REQUEST -> {
			return getAcknowledgeableRequestMagicBytes();
		}
		case ACKNOWLEDGEABLE_RESPONSE -> {
			return getAcknowledgeableResponseMagicBytes();
		}
		case ACKNOWLEDGEABLE_TRANSMISSION -> {
			return getAcknowledgeableTransmissionMagicBytes();
		}
		case REQUEST -> {
			return getRequestMagicBytes();
		}
		case RESPONSE -> {
			return getResponseMagicBytes();
		}
		case TRANSMISSION -> {
			return getTransmissionMagicBytes();
		}
		case PING -> {
			return getPingMagicBytes();
		}
		}
		return null;
	}

	/**
	 * Creates builder to build {@link TransmissionMetrics}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void validateMagicBytes() {
		final Map<Object, byte[]> theValidationMap = new HashMap<>();
		for ( AcknowledgeType eType : AcknowledgeType.values() ) {
			for ( Object eKey : theValidationMap.keySet() ) {
				if ( Arrays.equals( theValidationMap.get( eKey ), toMagicBytes( eType ) ) ) {
					throw new IllegalArgumentException( "The magic bytes (" + Arrays.toString( theValidationMap.get( eKey ) ) + ") assigned to <" + eKey.getClass().getSimpleName() + "." + eKey.toString() + "> must not be the same as the magic bytes (" + Arrays.toString( toMagicBytes( eType ) ) + ") assigned to <" + eType.getClass().getSimpleName() + "." + eType.toString() + ">!" );
				}
			}
			theValidationMap.put( eType, toMagicBytes( eType ) );
		}
		for ( TransmissionType eType : TransmissionType.values() ) {
			for ( Object eKey : theValidationMap.keySet() ) {
				if ( Arrays.equals( theValidationMap.get( eKey ), toMagicBytes( eType ) ) ) {
					throw new IllegalArgumentException( "The magic bytes (" + Arrays.toString( theValidationMap.get( eKey ) ) + ") assigned to <" + eKey.getClass().getSimpleName() + "." + eKey.toString() + "> must not be the same as the magic bytes (" + Arrays.toString( toMagicBytes( eType ) ) + ") assigned to <" + eType.getClass().getSimpleName() + "." + eType.toString() + ">!" );
				}
			}
			theValidationMap.put( eType, toMagicBytes( eType ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Builder to build {@link TransmissionMetrics}.
	 */
	public static class Builder extends TransmissionMetrics.Builder implements ReplyRetryNumberBuilder<Builder>, ReplyTimeoutMillisBuilder<Builder> {

		protected byte[] acknowledgeableTransmissionMagicBytes = DEFAULT_ACKNOWLEDGEABLE_TRANSMISSION_MAGIC_BYTES;
		protected byte[] acknowledgeableResponseMagicBytes = DEFAULT_ACKNOWLEDGEABLE_RESPONSE_MAGIC_BYTES;
		protected byte[] acknowledgeableRequestMagicBytes = DEFAULT_ACKNOWLEDGEABLE_REQUEST_MAGIC_BYTES;
		protected byte[] transmissionMagicBytes = DEFAULT_TRANSMISSION_MAGIC_BYTES;
		protected byte[] responseMagicBytes = DEFAULT_RESPONSE_MAGIC_BYTES;
		protected byte[] requestMagicBytes = DEFAULT_REQUEST_MAGIC_BYTES;
		protected byte[] transmissionDismissedMagicBytes = DEFAULT_TRANSMISSION_DISMISSED_MAGIC_BYTES;
		protected byte[] requestDismissedMagicBytes = DEFAULT_REQUEST_DISMISSED_MAGIC_BYTES;
		protected byte[] responseAcknowledgeMagicBytes = DEFAULT_RESPONSE_ACKNOWLEDGE_MAGIC_BYTES;

		protected int replyRetryNumber = DEFAULT_REPLY_RETRY_NUMBER;
		protected long replyTimeoutInMs = DEFAULT_REPLY_TIMEOUT_IN_MS;

		/**
		 * Instantiates a new builder.
		 */
		protected Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReplyRetryNumber( int aReplyRetryNumber ) {
			replyRetryNumber = aReplyRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReplyTimeoutMillis( long aReplyTimeoutInMs ) {
			replyTimeoutInMs = aReplyTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveInMs ) {
			ioHeuristicsTimeToLiveInMs = aIoHeuristicsTimeToLiveInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPongMagicBytes( byte[] aPongMagicBytes ) {
			pongMagicBytes = aPongMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingMagicBytes( byte[] aPingMagicBytes ) {
			pingMagicBytes = aPingMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingRetryNumber( int aPingRetryNumber ) {
			pingRetryNumber = aPingRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingTimeoutMillis( long aPingTimeoutInMs ) {
			pingTimeoutInMs = aPingTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			acknowledgeMagicBytes = aAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendMagicBytes( byte[] aClearToSendMagicBytes ) {
			clearToSendMagicBytes = aClearToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketMagicBytes( byte[] aPacketMagicBytes ) {
			packetMagicBytes = aPacketMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveMagicBytes( byte[] aReadyToReceiveMagicBytes ) {
			readyToReceiveMagicBytes = aReadyToReceiveMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes ) {
			readyToSendMagicBytes = aReadyToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeRetryNumber( int aAcknowledgeRetryNumber ) {
			acknowledgeRetryNumber = aAcknowledgeRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			acknowledgeSegmentPackager = aAcknowledgeSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutInMs ) {
			acknowledgeTimeoutInMs = aAcknowledgeTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBlockSize( int aBlockSize ) {
			blockSize = aBlockSize;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode ) {
			checksumValidationMode = aChecksumValidationMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager ) {
			clearToSendSegmentPackager = aClearToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendTimeoutMillis( long aClearToSendTimeoutInMs ) {
			clearToSendTimeoutInMs = aClearToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			crcAlgorithm = aCrcAlgorithm;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcChecksumConcatenateMode( ConcatenateMode aChecksumConcatenateMode ) {
			crcChecksumConcatenateMode = aChecksumConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEncoding( Charset aEncoding ) {
			encoding = aEncoding;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndOfStringByte( byte aEndOfStringByte ) {
			endOfStringByte = aEndOfStringByte;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEnquiryStandbyTimeMillis( long aEnquiryStandbyTimeInMs ) {
			enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLengthWidth( int aLengthWidth ) {
			lengthWidth = aLengthWidth;
			return this;
		}

		/**
		 * With packet length width.
		 *
		 * @param aPacketLengthWidth the packet length width
		 * 
		 * @return the builder
		 */
		@Override
		public Builder withPacketLengthWidth( int aPacketLengthWidth ) {
			packetLengthWidth = aPacketLengthWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withMagicBytesLength( int aMagicBytesLength ) {
			magicBytesLength = aMagicBytesLength;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketSegmentPackager( SegmentPackager aPacketSegmentPackager ) {
			packetSegmentPackager = aPacketSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadTimeoutMillis( long aReadTimeoutInMs ) {
			readTimeoutInMs = aReadTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveRetryNumber( int aReadyToReceiveRetryNumber ) {
			readyToReceiveRetryNumber = aReadyToReceiveRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager ) {
			readyToReceiveSegmentPackager = aReadyToReceiveSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutInMs ) {
			readyToReceiveTimeoutInMs = aReadyToReceiveTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendRetryNumber( int aReadyToSendRetryNumber ) {
			readyToSendRetryNumber = aReadyToSendRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager ) {
			readyToSendSegmentPackager = aReadyToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendTimeoutMillis( long aReadyToSendTimeoutInMs ) {
			readyToSendTimeoutInMs = aReadyToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode ) {
			sequenceNumberConcatenateMode = aSequenceNumberConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			sequenceNumberInitValue = aSequenceNumberInitValue;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberWidth( int aSequenceNumberWidth ) {
			sequenceNumberWidth = aSequenceNumberWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionRetryNumber( int aTransmissionRetryNumber ) {
			transmissionRetryNumber = aTransmissionRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionTimeoutMillis( long aTransmissionTimeoutInMs ) {
			transmissionTimeoutInMs = aTransmissionTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withWriteTimeoutMillis( long aWriteTimeoutInMs ) {
			writeTimeoutInMs = aWriteTimeoutInMs;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aAcknowledgeableTransmissionMagicBytes The according magic
		 *        bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withAcknowledgeableTransmissionMagicBytes( byte[] aAcknowledgeableTransmissionMagicBytes ) {
			acknowledgeableTransmissionMagicBytes = aAcknowledgeableTransmissionMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aAcknowledgeableResponseMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withAcknowledgeableResponseMagicBytes( byte[] aAcknowledgeableResponseMagicBytes ) {
			acknowledgeableResponseMagicBytes = aAcknowledgeableResponseMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aAcknowledgeableRequestMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withAcknowledgeableRequestMagicBytes( byte[] aAcknowledgeableRequestMagicBytes ) {
			acknowledgeableRequestMagicBytes = aAcknowledgeableRequestMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aTransmissionMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withTransmissionMagicBytes( byte[] aTransmissionMagicBytes ) {
			transmissionMagicBytes = aTransmissionMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 *
		 * @param aResponseMagicBytes the response magic bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withResponseMagicBytes( byte[] aResponseMagicBytes ) {
			responseMagicBytes = aResponseMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 *
		 * @param aResponseAcknowledgeMagicBytes the response acknowledge magic
		 *        bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withResponseAcknowledgeMagicBytes( byte[] aResponseAcknowledgeMagicBytes ) {
			responseAcknowledgeMagicBytes = aResponseAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aRequestMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withRequestMagicBytes( byte[] aRequestMagicBytes ) {
			requestMagicBytes = aRequestMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aTransmissionDismissedMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withTransmissionDismissedMagicBytes( byte[] aTransmissionDismissedMagicBytes ) {
			transmissionDismissedMagicBytes = aTransmissionDismissedMagicBytes;
			return this;
		}

		/**
		 * Sets the according magic bytes.
		 * 
		 * @param aRequestDismissedMagicBytes The according magic bytes.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withRequestDismissedMagicBytes( byte[] aRequestDismissedMagicBytes ) {
			requestDismissedMagicBytes = aRequestDismissedMagicBytes;
			return this;
		}

		/**
		 * Builds the {@link HandshakeTransmissionMetrics} instance from this
		 * builder's settings.
		 *
		 * @return The accordingly built {@link HandshakeTransmissionMetrics}
		 *         instance.
		 */
		@Override
		public HandshakeTransmissionMetrics build() {
			return new HandshakeTransmissionMetrics( this );
		}
	}
}
