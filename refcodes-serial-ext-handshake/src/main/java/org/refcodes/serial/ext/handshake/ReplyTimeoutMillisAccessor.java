// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

/**
 * Provides an accessor for a reply timeout in milliseconds property.
 */
public interface ReplyTimeoutMillisAccessor {

	/**
	 * The reply timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	long getReplyTimeoutMillis();

	/**
	 * Provides a mutator for a reply timeout in milliseconds property.
	 */
	public interface ReplyTimeoutMillisMutator {

		/**
		 * The reply timeout attribute in milliseconds.
		 * 
		 * @param aReplyTimeoutMillis An integer with the reply timeout in
		 *        milliseconds.
		 */
		void setReplyTimeoutMillis( long aReplyTimeoutMillis );
	}

	/**
	 * Provides a builder method for the reply timeout property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReplyTimeoutMillisBuilder<B extends ReplyTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the reply timeout property.
		 * 
		 * @param aReplyTimeoutMillis The reply timeout in milliseconds to be
		 *        stored by the reply timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReplyTimeoutMillis( long aReplyTimeoutMillis );
	}

	/**
	 * Provides a reply timeout in milliseconds property.
	 */
	public interface ReplyTimeoutMillisProperty extends ReplyTimeoutMillisAccessor, ReplyTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setReplyTimeoutMillis(long)} and returns the very same value
		 * (getter).
		 * 
		 * @param aReplyTimeoutMillis The long to set (via
		 *        {@link #setReplyTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letReplyTimeoutMillis( long aReplyTimeoutMillis ) {
			setReplyTimeoutMillis( aReplyTimeoutMillis );
			return aReplyTimeoutMillis;
		}
	}
}
