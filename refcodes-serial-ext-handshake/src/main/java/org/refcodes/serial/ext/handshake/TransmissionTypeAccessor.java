// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

/**
 * Provides an accessor for a {@link TransmissionType} property.
 */
public interface TransmissionTypeAccessor {

	/**
	 * Retrieves the {@link TransmissionType} from the {@link TransmissionType}
	 * property.
	 * 
	 * @return The {@link TransmissionType} stored by the
	 *         {@link TransmissionType} property.
	 */
	TransmissionType getTransmissionType();

	/**
	 * Provides a mutator for a {@link TransmissionType} property.
	 */
	public interface TransmissionTypeMutator {

		/**
		 * Sets the {@link TransmissionType} for the {@link TransmissionType}
		 * property.
		 * 
		 * @param aTransmissionType The {@link TransmissionType} to be stored by
		 *        the {@link TransmissionType} property.
		 */
		void setTransmissionType( TransmissionType aTransmissionType );
	}

	/**
	 * Provides a builder method for a {@link TransmissionType} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TransmissionTypeBuilder<B extends TransmissionTypeBuilder<B>> {

		/**
		 * Sets the {@link TransmissionType} for the {@link TransmissionType}
		 * property.
		 * 
		 * @param aTransmissionType The {@link TransmissionType} to be stored by
		 *        the {@link TransmissionType} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTransmissionType( TransmissionType aTransmissionType );
	}

	/**
	 * Provides a {@link TransmissionType} property.
	 */
	public interface TransmissionTypeProperty extends TransmissionTypeAccessor, TransmissionTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TransmissionType}
		 * (setter) as of {@link #setTransmissionType(TransmissionType)} and
		 * returns the very same value (getter).
		 * 
		 * @param aTransmissionType The {@link TransmissionType} to set (via
		 *        {@link #setTransmissionType(TransmissionType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TransmissionType letTransmissionType( TransmissionType aTransmissionType ) {
			setTransmissionType( aTransmissionType );
			return aTransmissionType;
		}
	}
}
