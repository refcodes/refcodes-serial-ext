// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

/**
 * Provides an accessor for a {@link AcknowledgeType} property.
 */
public interface AcknowledgeTypeAccessor {

	/**
	 * Retrieves the {@link AcknowledgeType} from the {@link AcknowledgeType}
	 * property.
	 * 
	 * @return The {@link AcknowledgeType} stored by the {@link AcknowledgeType}
	 *         property.
	 */
	AcknowledgeType getAcknowledgeType();

	/**
	 * Provides a mutator for a {@link AcknowledgeType} property.
	 */
	public interface AcknowledgeTypeMutator {

		/**
		 * Sets the {@link AcknowledgeType} for the {@link AcknowledgeType}
		 * property.
		 * 
		 * @param aAcknowledgeType The {@link AcknowledgeType} to be stored by
		 *        the {@link AcknowledgeType} property.
		 */
		void setAcknowledgeType( AcknowledgeType aAcknowledgeType );
	}

	/**
	 * Provides a builder method for a {@link AcknowledgeType} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AcknowledgeTypeBuilder<B extends AcknowledgeTypeBuilder<B>> {

		/**
		 * Sets the {@link AcknowledgeType} for the {@link AcknowledgeType}
		 * property.
		 * 
		 * @param aAcknowledgeType The {@link AcknowledgeType} to be stored by
		 *        the {@link AcknowledgeType} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAcknowledgeType( AcknowledgeType aAcknowledgeType );
	}

	/**
	 * Provides a {@link AcknowledgeType} property.
	 */
	public interface AcknowledgeTypeProperty extends AcknowledgeTypeAccessor, AcknowledgeTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link AcknowledgeType}
		 * (setter) as of {@link #setAcknowledgeType(AcknowledgeType)} and
		 * returns the very same value (getter).
		 * 
		 * @param aAcknowledgeType The {@link AcknowledgeType} to set (via
		 *        {@link #setAcknowledgeType(AcknowledgeType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default AcknowledgeType letAcknowledgeType( AcknowledgeType aAcknowledgeType ) {
			setAcknowledgeType( aAcknowledgeType );
			return aAcknowledgeType;
		}
	}
}
