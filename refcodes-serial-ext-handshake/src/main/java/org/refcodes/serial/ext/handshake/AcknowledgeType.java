// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

/**
 * The {@link AcknowledgeType} defines the type of an {@link AcknowledgeMessage}
 * confirmation mode in response to a {@link TransmissionMessage}.
 */
public enum AcknowledgeType {

	/**
	 * Sent when a transmission was received.
	 */
	ACKNOWLEDGE,

	/**
	 * Sent in case a transmission was received and dismissed by a receiver.
	 */
	TRANSMISSION_DISMISSED,

	/**
	 * A request is acknowledged by a response which in turn is acknowledged.
	 */
	RESPONSE,

	/**
	 * Sent when a request was received but dismissed by a receiver .
	 */
	REQUEST_DISMISSED,

	/**
	 * Identifies a a fire-and-forget pong (not to be acknowledged) reply in
	 * response to a simple ping request.
	 */
	PONG;

}
