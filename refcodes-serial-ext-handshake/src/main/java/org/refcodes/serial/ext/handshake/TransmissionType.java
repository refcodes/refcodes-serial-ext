// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

/**
 * The {@link TransmissionType} defines the type of a
 * {@link TransmissionMessage} and therewith the mode of operation of a
 * transmission process.
 */
public enum TransmissionType {

	/**
	 * Identifies a fire-and-forget transmission (no acknowledge sent).
	 */
	TRANSMISSION(false),

	/**
	 * Identifies a request expecting a fire-and-forget response (no acknowledge
	 * to be sent for the succeeding response).
	 */
	REQUEST(false),

	/**
	 * Identifies a fire-and-forget response in reply to a request (no
	 * acknowledge to be sent for the response).
	 */
	RESPONSE(false),

	/**
	 * Identifies an acknowledgeable transmission (acknowledge to be sent).
	 */
	ACKNOWLEDGEABLE_TRANSMISSION(true),

	/**
	 * Identifies a request expecting an acknowledgeable response (acknowledge
	 * to be sent sent for the succeeding response).
	 */
	ACKNOWLEDGEABLE_REQUEST(true),

	/**
	 * Identifies an acknowledgeable response in reply to a request (acknowledge
	 * to be sent for the response).
	 */
	ACKNOWLEDGEABLE_RESPONSE(true),

	/**
	 * Identifies a simple ping request to be responded by a fire-and-forget
	 * pong (not to be acknowledged).
	 */
	PING(true);

	private boolean _isAcknowledgeable;

	private TransmissionType( boolean isAcknowledgeable ) {
		_isAcknowledgeable = isAcknowledgeable;
	}

	/**
	 * Determines whether the {@link TransmissionType} expects an acknowledge.
	 * 
	 * @return True in case an acknowledge is expected, else false.
	 */
	public boolean isAcknowledgeable() {
		return _isAcknowledgeable;
	}
}
