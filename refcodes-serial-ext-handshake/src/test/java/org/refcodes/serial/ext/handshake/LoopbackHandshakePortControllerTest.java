// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.IntSegment;
import org.refcodes.serial.PayloadSegment;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortTestBench;
import org.refcodes.serial.Segment;
import org.refcodes.serial.SegmentComposite;
import org.refcodes.serial.SegmentResult;
import org.refcodes.serial.StringSegment;

public class LoopbackHandshakePortControllerTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static {
		final Logger root = Logger.getLogger( "" );
		root.setLevel( Level.SEVERE );
		for ( Handler handler : root.getHandlers() ) {
			handler.setLevel( Level.SEVERE );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("To confirm / debug blocking behavior.")
	@Test
	public void testEdgeCase1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theRD = assertMagicBytesSegment( segmentComposite( stringSegment(), intSegment() ), "FDX".getBytes() );
				theHandshakeReceivePort.receiveSegment( theRD );
			}
		}
	}

	@Test
	public void testPrimitive1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Random theRnd = new Random();
				byte eSend;
				byte eReceive;
				for ( int i = 0; i < 20; i++ ) {
					eSend = (byte) theRnd.nextInt();
					theHandshakeTransmitPort.transmitByte( eSend );
					eReceive = theHandshakeReceivePort.receiveByte();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Sent     = " + eSend );
						System.out.println( "Received = " + eReceive );
					}
					assertEquals( eSend, eReceive );
				}
			}
		}
	}

	@Test
	public void testPrimitive2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				byte[] theSent = Text.ARECIBO_MESSAGE.getText().getBytes();
				byte[] eReceived;
				for ( int i = 0; i < 20; i++ ) {
					theHandshakeTransmitPort.transmitBytes( theSent );
					eReceived = theHandshakeReceivePort.receiveAllBytes();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Message  = " + new String( theSent ) );
						System.out.println( "Received = " + new String( eReceived ) );
					}
					assertArrayEquals( theSent, eReceived );
				}
			}
		}
	}

	@Test
	public void testPrimitive3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				byte[] theSent = Text.ARECIBO_MESSAGE.getText().getBytes();
				byte[] theExpected = new byte[47];
				System.arraycopy( theSent, 0, theExpected, 0, 47 );
				byte[] eReceived;
				for ( int i = 0; i < 20; i++ ) {
					theHandshakeTransmitPort.transmitBytes( theSent, 0, 47 );
					eReceived = theHandshakeReceivePort.receiveBytes( 47 );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Message  = " + new String( theExpected ) );
						System.out.println( "Received = " + new String( eReceived ) );
					}
					assertArrayEquals( theExpected, eReceived );
				}
			}
		}
	}

	@Test
	public void testPrimitive4() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				byte[] theSent = Text.ARECIBO_MESSAGE.getText().getBytes();
				byte[] theExpected = new byte[47];
				byte[] eReceived = new byte[47];
				System.arraycopy( theSent, 0, theExpected, 0, 47 );
				byte[] theReceived = new byte[1024];
				for ( int i = 0; i < 20; i++ ) {
					theHandshakeTransmitPort.transmitBytes( theSent, 0, 47 );
					theHandshakeReceivePort.receiveBytes( theReceived, 128, 47 );
					System.arraycopy( theReceived, 128, eReceived, 0, 47 );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Message  = " + new String( theExpected ) );
						System.out.println( "Received = " + new String( eReceived ) );
					}
					assertArrayEquals( theExpected, eReceived );
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testPing() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				theHandshakeTransmitPort.ping();
			}
		}
	}

	@Test
	public void testTransmission1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText;
				final IntSegment theValue;
				final Segment theRD = assertMagicBytesSegment( segmentComposite( theText = stringSegment(), theValue = intSegment() ), "FDX".getBytes() );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
				theHandshakeTransmitPort.transmitSegment( theTD, false );
				theHandshakeReceivePort.receiveSegment( theRD );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
			}
		}
	}

	@Test
	public void testTransmission2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText;
				final IntSegment theValue;
				final Segment theRD = assertMagicBytesSegment( segmentComposite( theText = stringSegment(), theValue = intSegment() ), "FDX".getBytes() );
				final SegmentResult<?> theResult = theHandshakeReceivePort.onReceiveSegment( theRD );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
				theHandshakeTransmitPort.transmitSegment( theTD );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
			}
		}
	}

	@Test
	public void testTransmission3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD1 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText1;
				final IntSegment theValue1;
				final Segment theRD1 = assertMagicBytesSegment( segmentComposite( theText1 = stringSegment(), theValue1 = intSegment() ), "FDX".getBytes() );
				final Segment theTD2 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hello World!" ), intSegment( 1234 ) ), "FDX".getBytes() );
				final StringSegment theText2;
				final IntSegment theValue2;
				final Segment theRD2 = assertMagicBytesSegment( segmentComposite( theText2 = stringSegment(), theValue2 = intSegment() ), "FDX".getBytes() );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Text1 = " + theText1.getPayload() );
					System.out.println( "Value1 = " + theValue1.getPayload() );
					System.out.println( "Text2 = " + theText2.getPayload() );
					System.out.println( "Value2 = " + theValue2.getPayload() );
				}
				theHandshakeTransmitPort.transmitSegment( theTD1 );
				theHandshakeReceivePort.transmitSegment( theTD2 );
				theHandshakeReceivePort.receiveSegment( theRD1 );
				theHandshakeTransmitPort.receiveSegment( theRD2 );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Text1 = " + theText1.getPayload() );
					System.out.println( "Value1 = " + theValue1.getPayload() );
					System.out.println( "Text2 = " + theText2.getPayload() );
					System.out.println( "Value2 = " + theValue2.getPayload() );
				}
			}
		}
	}

	@Test
	public void testTransmission4() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final String theText1 = "Hallo Welt!";
				final String theText2 = "Hello World!";
				final int theValue1 = 5161;
				final int theValue2 = 1234;
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD1 = assertMagicBytesSegment( segmentComposite( stringSegment( theText1 ), intSegment( theValue1 ) ), "FDX".getBytes() );
				final StringSegment theTextSegment1;
				final IntSegment theValueSegment1;
				final Segment theRD1 = assertMagicBytesSegment( segmentComposite( theTextSegment1 = stringSegment(), theValueSegment1 = intSegment() ), "FDX".getBytes() );
				final Segment theTD2 = assertMagicBytesSegment( segmentComposite( stringSegment( theText2 ), intSegment( theValue2 ) ), "FDX".getBytes() );
				final StringSegment theTextSegment2;
				final IntSegment theValueSegment2;
				final Segment theRD2 = assertMagicBytesSegment( segmentComposite( theTextSegment2 = stringSegment(), theValueSegment2 = intSegment() ), "FDX".getBytes() );
				String eText1;
				Integer eValue1;
				String eText2;
				Integer eValue2;
				for ( int i = 0; i < 20; i++ ) {
					eText1 = theTextSegment1.getPayload();
					eValue1 = theValueSegment1.getPayload();
					eText2 = theTextSegment2.getPayload();
					eValue2 = theValueSegment2.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Text1 = " + eText1 );
						System.out.println( "Value1 = " + eValue1 );
						System.out.println( "Text2 = " + eText2 );
						System.out.println( "Value2 = " + eValue2 );
					}
					assertEquals( null, eText1 );
					assertEquals( null, eText2 );
					assertEquals( 0, eValue1 );
					assertEquals( 0, eValue2 );
					theHandshakeTransmitPort.transmitSegment( theTD1 );
					theHandshakeReceivePort.transmitSegment( theTD2 );
					theHandshakeReceivePort.receiveSegment( theRD1 );
					theHandshakeTransmitPort.receiveSegment( theRD2 );
					eText1 = theTextSegment1.getPayload();
					eValue1 = theValueSegment1.getPayload();
					eText2 = theTextSegment2.getPayload();
					eValue2 = theValueSegment2.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "--------------------------------------------------------------------------------" );
						System.out.println( "Text1 = " + eText1 );
						System.out.println( "Value1 = " + eValue1 );
						System.out.println( "Text2 = " + eText2 );
						System.out.println( "Value2 = " + eValue2 );
					}
					assertEquals( theText1, eText1 );
					assertEquals( theText2, eText2 );
					assertEquals( theValue1, eValue1 );
					assertEquals( theValue2, eValue2 );
					theRD1.reset();
					theRD2.reset();
				}
			}
		}
	}

	@Test
	public void testRequestResponse1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText;
				final StringSegment theRequestText;
				final IntSegment theResponseInt;
				final IntSegment theRequestInt;
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( theResponseText = stringSegment(), theResponseInt = intSegment() );
				theHandshakeReceivePort.onRequest( segmentComposite( theRequestText = stringSegment(), theRequestInt = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText.getPayload() ).reverse().toString() ), intSegment( theRequestInt.getPayload() + 1 ) ) );
				thePortTestBench.waitShortestForPortCatchUp();
				theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver, false );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
				}
				assertEquals( 5162, theResponseInt.getPayload() );
				assertEquals( "!tleW ollaH", theResponseText.getPayload() );
			}
		}
	}

	@Test
	public void testRequestResponse2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText;
				final StringSegment theRequestText;
				final IntSegment theResponseInt;
				final IntSegment theRequestInt;
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( theResponseText = stringSegment(), theResponseInt = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite = segmentComposite( theRequestText = stringSegment(), theRequestInt = intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
					}
					assertEquals( 0, theResponseInt.getPayload() );
					assertEquals( null, theResponseText.getPayload() );
					theHandshakeReceivePort.onRequest( theRequestSegmentComposite, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText.getPayload() ).reverse().toString() ), intSegment( theRequestInt.getPayload() + 1 ) ) );
					thePortTestBench.waitShortestForPortCatchUp();
					theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver, false );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "--------------------------------------------------------------------------------" );
						System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
					}
					assertEquals( 5162, theResponseInt.getPayload() );
					assertEquals( "!tleW ollaH", theResponseText.getPayload() );
					theResponseReceiver.reset();
				}
			}
		}
	}

	@Test
	public void testRequestResponse3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText1;
				final StringSegment theRequestText1;
				final IntSegment theResponseInt1;
				final IntSegment theRequestInt1;
				final Segment theRequestSender1 = segmentComposite( stringSegment( "Hallo Welt A!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver1 = segmentComposite( theResponseText1 = stringSegment(), theResponseInt1 = intSegment() );
				final StringSegment theResponseText2;
				final StringSegment theRequestText2;
				final IntSegment theResponseInt2;
				final IntSegment theRequestInt2;
				final Segment theRequestSender2 = segmentComposite( stringSegment( "Hallo Welt B!" ), intSegment( 6151 ) );
				final Segment theResponseReceiver2 = segmentComposite( theResponseText2 = stringSegment(), theResponseInt2 = intSegment() );
				theHandshakeReceivePort.onRequest( segmentComposite( theRequestText1 = stringSegment(), theRequestInt1 = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText1.getPayload() ).reverse().toString() ), intSegment( theRequestInt1.getPayload() + 1 ) ) );
				theHandshakeTransmitPort.onRequest( segmentComposite( theRequestText2 = stringSegment(), theRequestInt2 = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText2.getPayload() ).reverse().toString() ), intSegment( theRequestInt2.getPayload() + 1 ) ) );
				thePortTestBench.waitShortestForPortCatchUp();
				theHandshakeTransmitPort.requestSegment( theRequestSender1, theResponseReceiver1, false );
				theHandshakeReceivePort.requestSegment( theRequestSender2, theResponseReceiver2, false );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "1) " + theResponseText1.getPayload() + ", " + theResponseInt1.getPayload() );
					System.out.println( "2) " + theResponseText2.getPayload() + ", " + theResponseInt2.getPayload() );
				}
				assertEquals( 5162, theResponseInt1.getPayload() );
				assertEquals( "!A tleW ollaH", theResponseText1.getPayload() );
				assertEquals( 6152, theResponseInt2.getPayload() );
				assertEquals( "!B tleW ollaH", theResponseText2.getPayload() );
			}
		}
	}

	@Test
	public void testRequestResponse4() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText1;
				final StringSegment theRequestText1;
				final IntSegment theResponseInt1;
				final IntSegment theRequestInt1;
				final Segment theRequestSender1 = segmentComposite( stringSegment( "Hallo Welt A!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver1 = segmentComposite( theResponseText1 = stringSegment(), theResponseInt1 = intSegment() );
				final StringSegment theResponseText2;
				final StringSegment theRequestText2;
				final IntSegment theResponseInt2;
				final IntSegment theRequestInt2;
				final Segment theRequestSender2 = segmentComposite( stringSegment( "Hallo Welt B!" ), intSegment( 6151 ) );
				final Segment theResponseReceiver2 = segmentComposite( theResponseText2 = stringSegment(), theResponseInt2 = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite1 = segmentComposite( theRequestText1 = stringSegment(), theRequestInt1 = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite2 = segmentComposite( theRequestText2 = stringSegment(), theRequestInt2 = intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					theHandshakeReceivePort.onRequest( theRequestSegmentComposite1, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText1.getPayload() ).reverse().toString() ), intSegment( theRequestInt1.getPayload() + 1 ) ) );
					theHandshakeTransmitPort.onRequest( theRequestSegmentComposite2, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText2.getPayload() ).reverse().toString() ), intSegment( theRequestInt2.getPayload() + 1 ) ) );
					theHandshakeTransmitPort.requestSegment( theRequestSender1, theResponseReceiver1, false );
					theHandshakeReceivePort.requestSegment( theRequestSender2, theResponseReceiver2, false );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "1) " + theResponseText1.getPayload() + ", " + theResponseInt1.getPayload() );
						System.out.println( "2) " + theResponseText2.getPayload() + ", " + theResponseInt2.getPayload() );
					}
					assertEquals( 5162, theResponseInt1.getPayload() );
					assertEquals( "!A tleW ollaH", theResponseText1.getPayload() );
					assertEquals( 6152, theResponseInt2.getPayload() );
					assertEquals( "!B tleW ollaH", theResponseText2.getPayload() );
					theRequestText1.setPayload( null );
					theRequestText2.setPayload( null );
					theResponseText1.setPayload( null );
					theResponseText2.setPayload( null );
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testRequestNoResponse1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( stringSegment(), intSegment() );
				try {
					theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver, false );
					fail( "Expected a <" + IOException.class.getName() + "> exception!" );
				}
				catch ( IOException expected ) {
					assertNotNull( expected.getCause() );
					assertEquals( IllegalArgumentException.class, expected.getCause().getClass() );
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testRequestNoResponse2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( stringSegment(), intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					try {
						theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver, false );
						fail( "Expected a <" + IOException.class.getName() + "> exception!" );
					}
					catch ( IOException expected ) {
						assertNotNull( expected.getCause() );
						assertEquals( IllegalArgumentException.class, expected.getCause().getClass() );
					}
				}
			}
		}
	}

	@Test
	public void testAcknowledagbeTransmission1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText;
				final IntSegment theValue;
				final Segment theRD = assertMagicBytesSegment( segmentComposite( theText = stringSegment(), theValue = intSegment() ), "FDX".getBytes() );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
				theHandshakeTransmitPort.transmitSegment( theTD );
				theHandshakeReceivePort.receiveSegment( theRD );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
			}
		}
	}

	@Test
	public void testAcknowledagbeTransmission2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText;
				final IntSegment theValue;
				final Segment theRD = assertMagicBytesSegment( segmentComposite( theText = stringSegment(), theValue = intSegment() ), "FDX".getBytes() );
				final SegmentResult<?> theResult = theHandshakeReceivePort.onReceiveSegment( theRD );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
				theHandshakeTransmitPort.transmitSegment( theTD );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println( "Text = " + theText.getPayload() );
					System.out.println( "Value = " + theValue.getPayload() );
				}
			}
		}
	}

	@Test
	public void testAcknowledagbeTransmission3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theTD1 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final StringSegment theText1;
				final IntSegment theValue1;
				final Segment theRD1 = assertMagicBytesSegment( segmentComposite( theText1 = stringSegment(), theValue1 = intSegment() ), "FDX".getBytes() );
				final Segment theTD2 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hello World!" ), intSegment( 1234 ) ), "FDX".getBytes() );
				final StringSegment theText2;
				final IntSegment theValue2;
				final Segment theRD2 = assertMagicBytesSegment( segmentComposite( theText2 = stringSegment(), theValue2 = intSegment() ), "FDX".getBytes() );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "################################################################################" );
					System.out.println( "Text1 = " + theText1.getPayload() );
					System.out.println( "Value1 = " + theValue1.getPayload() );
					System.out.println( "Text2 = " + theText2.getPayload() );
					System.out.println( "Value2 = " + theValue2.getPayload() );
				}
				assertEquals( null, theText1.getPayload() );
				assertEquals( 0, theValue1.getPayload() );
				assertEquals( null, theText2.getPayload() );
				assertEquals( 0, theValue1.getPayload() );
				theHandshakeTransmitPort.transmitSegment( theTD1 );
				theHandshakeReceivePort.transmitSegment( theTD2 );
				theHandshakeReceivePort.receiveSegment( theRD1 );
				theHandshakeTransmitPort.receiveSegment( theRD2 );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "--------------------------------------------------------------------------------" );
					System.out.println( "Text1 = " + theText1.getPayload() );
					System.out.println( "Value1 = " + theValue1.getPayload() );
					System.out.println( "Text2 = " + theText2.getPayload() );
					System.out.println( "Value2 = " + theValue2.getPayload() );
				}
				assertEquals( "Hallo Welt!", theText1.getPayload() );
				assertEquals( 5161, theValue1.getPayload() );
				assertEquals( "Hello World!", theText2.getPayload() );
				assertEquals( 1234, theValue2.getPayload() );
			}
		}
	}

	@Test
	public void testAcknowledagbeTransmission4() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theText1;
				final IntSegment theValue1;
				final StringSegment theText2;
				final IntSegment theValue2;
				final Segment theTD1 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) ), "FDX".getBytes() );
				final Segment theRD1 = assertMagicBytesSegment( segmentComposite( theText1 = stringSegment(), theValue1 = intSegment() ), "FDX".getBytes() );
				final Segment theTD2 = assertMagicBytesSegment( segmentComposite( stringSegment( "Hello World!" ), intSegment( 1234 ) ), "FDX".getBytes() );
				final Segment theRD2 = assertMagicBytesSegment( segmentComposite( theText2 = stringSegment(), theValue2 = intSegment() ), "FDX".getBytes() );
				for ( int i = 0; i < 20; i++ ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "Text1 = " + theText1.getPayload() );
						System.out.println( "Value1 = " + theValue1.getPayload() );
						System.out.println( "Text2 = " + theText2.getPayload() );
						System.out.println( "Value2 = " + theValue2.getPayload() );
					}
					assertEquals( null, theText1.getPayload() );
					assertEquals( 0, theValue1.getPayload() );
					assertEquals( null, theText2.getPayload() );
					assertEquals( 0, theValue2.getPayload() );
					theHandshakeTransmitPort.transmitSegment( theTD1 );
					theHandshakeReceivePort.transmitSegment( theTD2 );
					thePortTestBench.waitShortestForPortCatchUp();
					theHandshakeReceivePort.receiveSegment( theRD1 );
					theHandshakeTransmitPort.receiveSegment( theRD2 );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "--------------------------------------------------------------------------------" );
						System.out.println( "Text1 = " + theText1.getPayload() );
						System.out.println( "Value1 = " + theValue1.getPayload() );
						System.out.println( "Text2 = " + theText2.getPayload() );
						System.out.println( "Value2 = " + theValue2.getPayload() );
					}
					assertEquals( "Hallo Welt!", theText1.getPayload() );
					assertEquals( 5161, theValue1.getPayload() );
					assertEquals( "Hello World!", theText2.getPayload() );
					assertEquals( 1234, theValue2.getPayload() );
					theRD1.reset();
					theRD2.reset();
				}
			}
		}
	}

	@Test
	public void testAcknowledagbeRequestResponse1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText;
				final StringSegment theRequestText;
				final IntSegment theResponseInt;
				final IntSegment theRequestInt;
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( theResponseText = stringSegment(), theResponseInt = intSegment() );
				theHandshakeReceivePort.onRequest( segmentComposite( theRequestText = stringSegment(), theRequestInt = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText.getPayload() ).reverse().toString() ), intSegment( theRequestInt.getPayload() + 1 ) ) );
				thePortTestBench.waitShortestForPortCatchUp();
				theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
				}
				assertEquals( 5162, theResponseInt.getPayload() );
				assertEquals( "!tleW ollaH", theResponseText.getPayload() );
			}
		}
	}

	@Test
	public void testAcknowledagbeRequestResponse2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText;
				final StringSegment theRequestText;
				final IntSegment theResponseInt;
				final IntSegment theRequestInt;
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( theResponseText = stringSegment(), theResponseInt = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite = segmentComposite( theRequestText = stringSegment(), theRequestInt = intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
					}
					assertEquals( 0, theResponseInt.getPayload() );
					assertEquals( null, theResponseText.getPayload() );
					theHandshakeReceivePort.onRequest( theRequestSegmentComposite, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText.getPayload() ).reverse().toString() ), intSegment( theRequestInt.getPayload() + 1 ) ) );
					theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "--------------------------------------------------------------------------------" );
						System.out.println( theResponseText.getPayload() + ", " + theResponseInt.getPayload() );
					}
					assertEquals( 5162, theResponseInt.getPayload() );
					assertEquals( "!tleW ollaH", theResponseText.getPayload() );
					theResponseReceiver.reset();
				}
			}
		}
	}

	@Test
	public void testAcknowledagbeRequestResponse3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText1;
				final StringSegment theRequestText1;
				final IntSegment theResponseInt1;
				final IntSegment theRequestInt1;
				final Segment theRequestSender1 = segmentComposite( stringSegment( "Hallo Welt A!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver1 = segmentComposite( theResponseText1 = stringSegment(), theResponseInt1 = intSegment() );
				final StringSegment theResponseText2;
				final StringSegment theRequestText2;
				final IntSegment theResponseInt2;
				final IntSegment theRequestInt2;
				final Segment theRequestSender2 = segmentComposite( stringSegment( "Hallo Welt B!" ), intSegment( 6151 ) );
				final Segment theResponseReceiver2 = segmentComposite( theResponseText2 = stringSegment(), theResponseInt2 = intSegment() );
				theHandshakeReceivePort.onRequest( segmentComposite( theRequestText1 = stringSegment(), theRequestInt1 = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText1.getPayload() ).reverse().toString() ), intSegment( theRequestInt1.getPayload() + 1 ) ) );
				theHandshakeTransmitPort.onRequest( segmentComposite( theRequestText2 = stringSegment(), theRequestInt2 = intSegment() ), req -> segmentComposite( stringSegment( new StringBuilder( theRequestText2.getPayload() ).reverse().toString() ), intSegment( theRequestInt2.getPayload() + 1 ) ) );
				thePortTestBench.waitShortestForPortCatchUp();
				theHandshakeTransmitPort.requestSegment( theRequestSender1, theResponseReceiver1 );
				theHandshakeReceivePort.requestSegment( theRequestSender2, theResponseReceiver2 );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "1) " + theResponseText1.getPayload() + ", " + theResponseInt1.getPayload() );
					System.out.println( "2) " + theResponseText2.getPayload() + ", " + theResponseInt2.getPayload() );
				}
				assertEquals( 5162, theResponseInt1.getPayload() );
				assertEquals( "!A tleW ollaH", theResponseText1.getPayload() );
				assertEquals( 6152, theResponseInt2.getPayload() );
				assertEquals( "!B tleW ollaH", theResponseText2.getPayload() );
			}
		}
	}

	@Test
	public void testAcknowledagbeRequestResponse4() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final StringSegment theResponseText1;
				final StringSegment theRequestText1;
				final IntSegment theResponseInt1;
				final IntSegment theRequestInt1;
				final StringSegment theResponseText2;
				final StringSegment theRequestText2;
				final IntSegment theResponseInt2;
				final IntSegment theRequestInt2;
				final Segment theRequestSender2 = segmentComposite( stringSegment( "Hallo Welt B!" ), intSegment( 6151 ) );
				final Segment theRequestSender1 = segmentComposite( stringSegment( "Hallo Welt A!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver1 = segmentComposite( theResponseText1 = stringSegment(), theResponseInt1 = intSegment() );
				final Segment theResponseReceiver2 = segmentComposite( theResponseText2 = stringSegment(), theResponseInt2 = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite1 = segmentComposite( theRequestText1 = stringSegment(), theRequestInt1 = intSegment() );
				final SegmentComposite<PayloadSegment<? extends Object>> theRequestSegmentComposite2 = segmentComposite( theRequestText2 = stringSegment(), theRequestInt2 = intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "################################################################################" );
						System.out.println( "1) " + theResponseText1.getPayload() + ", " + theResponseInt1.getPayload() );
						System.out.println( "2) " + theResponseText2.getPayload() + ", " + theResponseInt2.getPayload() );
					}
					assertEquals( 0, theResponseInt1.getPayload() );
					assertEquals( null, theResponseText1.getPayload() );
					assertEquals( 0, theResponseInt2.getPayload() );
					assertEquals( null, theResponseText2.getPayload() );
					theHandshakeReceivePort.onRequest( theRequestSegmentComposite1, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText1.getPayload() ).reverse().toString() ), intSegment( theRequestInt1.getPayload() + 1 ) ) );
					theHandshakeTransmitPort.onRequest( theRequestSegmentComposite2, req -> segmentComposite( stringSegment( new StringBuilder( theRequestText2.getPayload() ).reverse().toString() ), intSegment( theRequestInt2.getPayload() + 1 ) ) );
					thePortTestBench.waitShortestForPortCatchUp();
					theHandshakeTransmitPort.requestSegment( theRequestSender1, theResponseReceiver1 );
					theHandshakeReceivePort.requestSegment( theRequestSender2, theResponseReceiver2 );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "--------------------------------------------------------------------------------" );
						System.out.println( "1) " + theResponseText1.getPayload() + ", " + theResponseInt1.getPayload() );
						System.out.println( "2) " + theResponseText2.getPayload() + ", " + theResponseInt2.getPayload() );
					}
					assertEquals( 5162, theResponseInt1.getPayload() );
					assertEquals( "!A tleW ollaH", theResponseText1.getPayload() );
					assertEquals( 6152, theResponseInt2.getPayload() );
					assertEquals( "!B tleW ollaH", theResponseText2.getPayload() );
					theResponseReceiver1.reset();
					theResponseReceiver2.reset();
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testAcknowledagbeRequestNoResponse1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( stringSegment(), intSegment() );
				try {
					theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver );
					fail( "Expected a <" + IOException.class.getName() + "> exception!" );
				}
				catch ( IOException expected ) {
					assertNotNull( expected.getCause() );
					assertEquals( IllegalArgumentException.class, expected.getCause().getClass() );
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testAcknowledagbeRequestNoResponse2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
				final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
				final Segment theRequestSender = segmentComposite( stringSegment( "Hallo Welt!" ), intSegment( 5161 ) );
				final Segment theResponseReceiver = segmentComposite( stringSegment(), intSegment() );
				for ( int i = 0; i < 10; i++ ) {
					try {
						theHandshakeTransmitPort.requestSegment( theRequestSender, theResponseReceiver );
						fail( "Expected a <" + IOException.class.getName() + "> exception!" );
					}
					catch ( IOException expected ) {
						assertNotNull( expected.getCause() );
						assertEquals( IllegalArgumentException.class, expected.getCause().getClass() );
					}
				}
			}
		}
	}
}
