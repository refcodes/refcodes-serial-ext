// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial.ext.handshake;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.serial.alt.tty.BaudRate;
import org.refcodes.serial.alt.tty.TtyPortMetrics;
import org.refcodes.serial.alt.tty.TtyPortTestBench;

public class TtyHandshakePortControllerTest extends LoopbackHandshakePortControllerTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static {
		final Logger root = Logger.getLogger( "" );
		root.setLevel( Level.SEVERE );
		for ( Handler handler : root.getHandlers() ) {
			handler.setLevel( Level.SEVERE );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("To confirm / debug blocking behavior.")
	@Override
	@Test
	public void testEdgeCase1() throws IOException {
		super.testEdgeCase1();
	}

	@Override
	@Test
	public void testPrimitive1() throws IOException {
		super.testPrimitive1();
	}

	@Override
	@Test
	public void testPrimitive2() throws IOException {
		super.testPrimitive2();
	}

	@Override
	@Test
	public void testPrimitive3() throws IOException {
		super.testPrimitive3();
	}

	@Override
	@Test
	public void testPrimitive4() throws IOException {
		super.testPrimitive4();
	}

	@Override
	@Test
	public void testPing() throws IOException {
		super.testPing();
	}

	@Override
	@Test
	public void testTransmission1() throws IOException {
		super.testTransmission1();
	}

	@Override
	@Test
	public void testTransmission2() throws IOException {
		super.testTransmission2();
	}

	@Override
	@Test
	public void testTransmission3() throws IOException {
		super.testTransmission3();
	}

	@Override
	@Test
	public void testTransmission4() throws IOException {
		super.testTransmission4();
	}

	@Override
	@Test
	public void testRequestResponse1() throws IOException {
		super.testRequestResponse1();
	}

	@Override
	@Test
	public void testRequestResponse2() throws IOException {
		super.testRequestResponse2();
	}

	@Override
	@Test
	public void testRequestResponse3() throws IOException {
		super.testRequestResponse3();
	}

	@Override
	@Test
	public void testRequestResponse4() throws IOException {
		super.testRequestResponse4();
	}

	@Override
	@Test
	public void testRequestNoResponse1() throws IOException {
		super.testRequestNoResponse1();
	}

	@Override
	@Test
	public void testRequestNoResponse2() throws IOException {
		super.testRequestNoResponse2();
	}

	@Override
	@Test
	public void testAcknowledagbeTransmission1() throws IOException {
		super.testAcknowledagbeTransmission1();
	}

	@Override
	@Test
	public void testAcknowledagbeTransmission2() throws IOException {
		super.testAcknowledagbeTransmission2();
	}

	@Override
	@Test
	public void testAcknowledagbeTransmission3() throws IOException {
		super.testAcknowledagbeTransmission3();
	}

	@Override
	@Test
	public void testAcknowledagbeTransmission4() throws IOException {
		super.testAcknowledagbeTransmission4();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestResponse1() throws IOException {
		super.testAcknowledagbeRequestResponse1();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestResponse2() throws IOException {
		super.testAcknowledagbeRequestResponse2();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestResponse3() throws IOException {
		super.testAcknowledagbeRequestResponse3();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestResponse4() throws IOException {
		super.testAcknowledagbeRequestResponse4();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestNoResponse1() throws IOException {
		super.testAcknowledagbeRequestNoResponse1();
	}

	@Override
	@Test
	public void testAcknowledagbeRequestNoResponse2() throws IOException {
		super.testAcknowledagbeRequestNoResponse2();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	protected TtyPortTestBench createPortTestBench() {
		return new TtyPortTestBench( TtyPortMetrics.builder().withBaudRate( BaudRate.BPS_14400 ).build() );
	}

}
